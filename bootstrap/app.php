<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(dirname(__DIR__)))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    dirname(__DIR__)
);

$app->withFacades();

// $app->withEloquent();

$app->configure('options');

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

//RRAP API service
$rrapApiConfig = [
    'base_uri' => env('RRAP_BASE_URI'),
    'content_type' => config('options.rrap_api.content_type'),
    'auth_type' => env('RRAP_AUTH_TYPE'),
    'auth_username' => env('RRAP_USERNAME'),
    'auth_password' => env('RRAP_PASSWORD'),
];
$app->instance('RrapApiService', new App\Services\ApiProxyService($rrapApiConfig));

//Contentful CMS API service
$contentfulService = new \Contentful\Delivery\Client(env('CONTENTFUL_TOKEN'),
                                                     env('CONTENTFUL_SPACE_ID'),
                                                     env('CONTENTFUL_ENVIRONMENT'));
$app->instance('cmsApiService', new App\Services\CmsApiService($contentfulService));

//AWS S3 service
$s3Client = new \Aws\S3\S3Client([
    'version' => 'latest',
    'region' => env('AWS_REGION'),
    'credentials' => [
        'key' => env('AWS_KEY'),
        'secret' => env('AWS_SECRET')
    ]
]);
$app->instance('awsS3Service', new App\Services\AwsS3Service($s3Client));

//Need to initialise here, in order for logging to stream to s3 bucket
$awsS3Service = app('awsS3Service');
$awsS3Service->getClient()->registerStreamWrapper();

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

// $app->middleware([
//     App\Http\Middleware\ExampleMiddleware::class
// ]);

$app->middleware([
    App\Http\Middleware\CorsMiddleware::class,
    App\Http\Middleware\JsonRequestMiddleware::class
]);

$app->routeMiddleware([
    'auth' => App\Http\Middleware\Authenticate::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

//$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
$app->register(Aws\Laravel\AwsServiceProvider::class);

// $app->register(App\Providers\EventServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/web.php';
});

return $app;
