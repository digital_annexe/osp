<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use App\Services\ApiProxyService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RootController;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api/v1',
                'middleware' => 'auth'], function($router)
{
    //RRAP API routers
    $router->group(['prefix' => 'rrap'], function($router)
    {
        //get retailer detail
        $router->get('retailer/{id}','Rrap\MainController@showRetailer');

        //get vehicle detail by id (not VIN or VID)
        $router->get('vehicle/{id}','Rrap\MainController@showVehicle');

        /**
         * get lookup lists
         * lookupLists?cache=reload will force reload the cache
         */
        $router->get('lookupLists','Rrap\MainController@showLookupLists');

        //search new vehicles
        $router->post('searchNewVehicles','Rrap\MainController@postSearchNewVehicles');

        //search used vehicles
        $router->post('searchUsedVehicles','Rrap\MainController@postSearchUsedVehicles');
    });

    //CMS API routers
    $router->group(['prefix' => 'cms'], function($router)
    {
        $router->get('getPageByName/{name}','Cms\MainController@showPage');

        $router->get('getCuratedSearchPageByName/{name}','Cms\MainController@showCuratedPage');

        $router->get('getSuperCarsPageByName/{name}','Cms\MainController@showSuperCarsPage');

    });

    //health checks
    $router->group(['prefix' => 'healthcheck'], function($router)
    {
        //general
        $router->get('/','HealthcheckController@showGeneral');

        //CMS API
        $router->get('cms','HealthcheckController@showCms');

        //RRAP API
        $router->get('rrap','HealthcheckController@showRrap');
    });
});

//root URL, showing all available routes
$router->get('/', ['middleware' => 'auth', function () {
    $controller = new RootController();
    return $controller->showAvailableRoutes();
}]);