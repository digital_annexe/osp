<?php

return [
    'general' => [
        's3_log_path' => 's3://' . env('AWS_BUCKET') . '/' . env('APP_ENV') . '/osp-' . env('APP_ENV') . '.log',
        'logger_time_format' => "Y-m-d H:i:s.u",
    ],
    'rrap_api' => [
        'content_type' => 'application/json',
        'cache_key' => [
            'lookup_list' => 'rrap_main_lookup_lists'
            ]
    ],
    'cms_api' => [
        'content_type' => 'application/json'
    ]
];