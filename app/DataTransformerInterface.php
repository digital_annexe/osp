<?php
namespace App;

interface DataTransformerInterface
{
    public function transform(): array;
}