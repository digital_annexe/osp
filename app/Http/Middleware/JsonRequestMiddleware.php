<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Libraries\JsonHelper;
use App\Http\Controllers\RouterLoggerTrait;

class JsonRequestMiddleware
{
    use RouterLoggerTrait;

    public function handle(Request $request, Closure $next)
    {
        if (in_array($request->method(), ['POST', 'PUT', 'PATCH'])
            && $request->isJson()
        ) {
            //Check if JSON is malformed before going to router
            if (! JsonHelper::isJsonValid($request->getContent())) {
                $this->logRoute();
                return response('JSON is malformed', Response::HTTP_BAD_REQUEST);
            }

            $data = $request->json()->all();
            $request->request->replace(is_array($data) ? $data : []);
        }
        return $next($request);
    }
}