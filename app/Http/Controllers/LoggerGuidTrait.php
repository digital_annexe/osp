<?php
namespace App\Http\Controllers;

use App\Libraries\StringHelper;

trait LoggerGuidTrait
{
    protected $loggerGuid = 'logger_guid';

    protected function generateNewLoggerGuid(): void
    {
        config(['options.general.logger_guid' => StringHelper::generateGuid()]);
    }

    protected function getLoggerGuid(): string
    {
        return config('options.general.logger_guid');

    }
}