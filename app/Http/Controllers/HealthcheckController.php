<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class HealthcheckController extends Controller
{
    use ResponseTrait;

    public function showGeneral()
    {
        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    public function showCms()
    {
        /** @var \App\Services\CmsApiServiceInterface */
        try {
            $cmsService = app('cmsApiService');
            $client = $cmsService->getClient();

            /*
             * Attempt to get top-level space (as defined in config),
             * to verify that a response has been received from API
             */
            $response = $client->getSpace()->jsonSerialize();
            $responseCode = Response::HTTP_NO_CONTENT;
        } catch (\Exception $exception) {
            $responseCode = Response::HTTP_SERVICE_UNAVAILABLE;
        }
        return response()->json(['cms'], $responseCode);
    }

    public function showRrap()
    {
        try {
            $instance = app('RrapApiService');
            $response = $instance->get('Ncl/v1/Session/');
            $responseCode = Response::HTTP_NO_CONTENT;
        } catch (\Exception $e) {
            $responseCode = Response::HTTP_SERVICE_UNAVAILABLE;
        }
        return response()->json(['rrap'], $responseCode);
    }
}