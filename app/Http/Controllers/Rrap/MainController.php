<?php
namespace App\Http\Controllers\Rrap;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseTrait;
use App\Http\Controllers\RouterLoggerTrait;
use Illuminate\Http\JsonResponse;
use App\Services\APIServiceInterface;
use Illuminate\Http\Request;
use App\SearchVehiclesDataTransformer;
use App\VehiclePdfLinkBuilder;
use App\Services\DataTransformerBuilder;
use App\CacheHandler;
use App\CacheHandlerInterface;
use App\Libraries\ValidationHelper;
use App\Libraries\ArrayHelper;

class MainController extends Controller
{
    use ResponseTrait;
    use RouterLoggerTrait;

    const QUERY_PARAM_RELOAD = 'reload';

    protected $cacheHandlerForLookupLists;

    /**
     * MainController constructor.
     */
    public function __construct()
    {
        $this->cacheHandlerForLookupLists = new CacheHandler(config('options.rrap_api.cache_key.lookup_list'));
    }

    public function showRetailer(int $id): JsonResponse
    {
        $this->logRoute();
        $response = $this->getService()->get("retailer/{$id}");
        if (empty($response)) {
            return $this->itemNotFoundResponse('Retailer', $id);
        }
        return $this->prepareResponse($response);
    }

    public function showVehicle(string $id): JsonResponse
    {
        $this->logRoute();
        $payload = ['VehicleId' => ValidationHelper::filterForInteger($id)];
        $vehicleResponse = $this->getService()->post("details", $payload);
        if (empty($vehicleResponse) || ! isset($vehicleResponse['Vehicles'][0])) {
            return $this->itemNotFoundResponse('Vehicle', $id);
        }

        $response['General'] = $vehicleResponse['Vehicles'][0];

        //get finance data, if VIN exists
        if (ArrayHelper::doesKeyExist('Vin', $response)) {
            $response['Finance'] = $this->getVehicleFinanceData($response['General']);

            //generate link to finance PDF
            if (ArrayHelper::doesKeyExist('FinanceProductResults', $response)) {
                $response['pdf'] = $this->generateVehiclePdfLink($response);
            }
        }
        return $this->prepareResponse($response);
    }

    public function showLookupLists(Request $request): JsonResponse
    {
        $this->logRoute();
        if ($request->get('cache') === self::QUERY_PARAM_RELOAD || ! $this->cacheHandlerForLookupLists->exists()) {
            $response = $this->getLatestLookupListsAndUpdateCache();
        } else {
            $response = $this->cacheHandlerForLookupLists->get();
        }
        return $this->prepareResponse($response);
    }

    public function postSearchNewVehicles(Request $request): JsonResponse
    {
        return $this->searchVehicles($request, 'search/new');
    }

    public function postSearchUsedVehicles(Request $request): JsonResponse
    {
        return $this->searchVehicles($request, 'search/used');
    }

    protected function getService(): APIServiceInterface
    {
        $instance = app('RrapApiService');
        return $instance;
    }

    protected function searchVehicles(Request $request, string $uri): JsonResponse
    {
        $this->logRoute();
        $dataTransformerBuilder = new DataTransformerBuilder($request->all());
        $transformer = new SearchVehiclesDataTransformer($dataTransformerBuilder);
        $payload = $transformer->transform();
        $response = $this->getService()->post($uri, $payload);
        return $this->prepareResponse($response);
    }

    protected function getLatestLookupListsAndUpdateCache(): array
    {
        $response = $this->getService()->get('filters');
        $this->cacheHandlerForLookupLists->set($response);
        $response = $this->cacheHandlerForLookupLists->get();
        return $response;
    }

    protected function getVehicleFinanceData(array $response): ?array
    {
        $financeResponse = null;
        if (isset($response['Vin'])) {
            $payload = ['Vins' => $response['Vin']];
            $financeResponse = $this->getService()->post("finance/calculate", $payload);
        }
        return $financeResponse;
    }

    protected function generateVehiclePdfLink(array $vehicleData): string
    {
        $baseUri = env('RRAP_BASE_URI') . '/details/pdf';
        $handler = new VehiclePdfLinkBuilder($vehicleData, $baseUri);
        return $handler->transform();
    }
}