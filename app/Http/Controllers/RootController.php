<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseTrait;

class RootController extends Controller
{
    use ResponseTrait;

    public function showAvailableRoutes()
    {
        $routes = ['Message' => 'The list below shows all available routes. Please refer to YFS OSP ' .
                                'documentation for more information',
                    'Configuration Environment' => strtoupper(env('APP_ENV'))
        ];
        foreach(\Route::getRoutes() as $route) {
            $item = [
                'Endpoint' => $route['uri'],
                'Method' => $route['method'],
                'Auth' => isset($route['action']['middleware']) ?? $route['action']['middleware'][0]
            ];
            $routes['AvailableRoutes'][] = $item;
        }
        return $this->prepareResponse($routes);
    }
}