<?php
namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseTrait;
use App\Http\Controllers\RouterLoggerTrait;
use Illuminate\Http\JsonResponse;
use App\Services\CmsApiServiceInterface;

class MainController extends Controller
{
    use ResponseTrait;
    use RouterLoggerTrait;

    public function showPage(string $name): JsonResponse
    {
        $this->logRoute();
        $response = $this->getService()->getPageByName($name);

        if (empty($response) || empty($response['items'])) {
            return $this->itemNotFoundResponse('Page', $name);
        }
        return $this->prepareResponse($response);
    }

    public function showCuratedPage(string $name): JsonResponse
    {
        $this->logRoute();
        $response = $this->getService()->getCuratedSearchPageByName($name);

        if (empty($response) || empty($response['items'])) {
            return $this->itemNotFoundResponse('Curated Search Page', $name);
        }
        return $this->prepareResponse($response);
    }

    public function showSuperCarsPage(string $name): JsonResponse
    {
        $this->logRoute();
        $response = $this->getService()->showSuperCarsPage($name);

        if (empty($response) || empty($response['items'])) {
            return $this->itemNotFoundResponse('Super Car Page', $name);
        }
        return $this->prepareResponse($response);
    }



    /**
     * @return \Laravel\Lumen\Application|mixed
     */
    protected function getService(): CmsApiServiceInterface
    {
        $instance = app('cmsApiService');
        return $instance;
    }
}