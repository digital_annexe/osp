<?php
namespace App\Http\Controllers;

use Log;
use App\Http\Controllers\LoggerGuidTrait;

trait RouterLoggerTrait
{
    use LoggerGuidTrait;

    protected function logRoute(string $message = null): void
    {
        /**
         * Generate a new global GUID so that we can group all requests made from
         * frontend->proxy->third party, and the response back from third party
         */
        $this->generateNewLoggerGuid();
        $request = app('request');
        $info = [
            'method' => $request->getMethod(),
            'query_params' => $request->input(),
            'guid' => $this->getLoggerGuid()
        ];
        if ($message) {
            $info['message'] = $message;
        }

        Log::info($request->fullUrl(), $info);
    }
}