<?php
namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

trait ResponseTrait
{
    protected function prepareResponse(array $data): JsonResponse
    {
        $response = null;

        switch (config('options.rrap_api.content_type')) {
            case 'application/json':
            default:
                $response = response()->json($data);
                break;
        }
        return $response;
    }

    protected function itemNotFoundResponse(string $item, string $id): JsonResponse
    {
        $response = [
            'Error' => "{$item} with ID '{$id}' not found."
        ];
        return response()->json($response, Response::HTTP_NOT_FOUND);
    }
}