<?php
namespace App\Logging;

use Monolog\Formatter\LineFormatter;

class CustomiseFormatter
{
    public function __invoke($logger)
    {
        foreach ($logger->getHandlers() as $handler) {
            $loggerFormat = "[%datetime%] %level_name% %message% %context% %extra%\n";
            $loggerTimeFormat = config('options.general.logger_time_format');

            $formatter = new LineFormatter($loggerFormat, $loggerTimeFormat);
            $formatter->ignoreEmptyContextAndExtra(true);
            $handler->setFormatter($formatter);
            $logger->pushHandler($handler);
        }
    }
}