<?php
namespace App;

use App\Services\DataTransformerBuilderInterface;
use App\Libraries\ValidationHelper;
use App\Libraries\ArrayHelper;
use App\Libraries\StringHelper;

class SearchVehiclesDataTransformer implements DataTransformerInterface
{
    protected $data = [];

    protected $dataTransformerBuilder;
    
    protected $flattenArrayAndValidateContentsAsNumbers = ['flattenArray', 'getArrayCleansedForNumbers'];

    public function __construct(DataTransformerBuilderInterface $dataTransformerBuilder)
    {
        $this->dataTransformerBuilder = $dataTransformerBuilder;
        $this->dataTransformerBuilder->setDataTransformer($this)
                                     ->setBase($this->getMaster());
    }

    public function transform(): array
    {
        $this->buildPaging();
        $this->buildCriteria();
        $this->buildFinanceCriteria();
        
        return $this->dataTransformerBuilder->render();
    }

    protected function getMaster(): array
    {
        $data = [
                    'Sort' => [],
                    'Paging' => [],
                    'MaxResults' => null,
                    'MonthlySort' => false
        ];
        return $data;
    }

    protected function buildPaging(): void
    {
        $this->dataTransformerBuilder->setTransformerMethods(['getPaging'])
                                     ->build('Paging', 'Paging');
        $this->dataTransformerBuilder->build('Paging.MaxResults', 'MaxResults');
    }

    protected function buildCriteria(): void
    {
        $this->dataTransformerBuilder->setTransformerMethods(['flattenArray'])
                                     ->build('Criteria.Vins', 'Criteria.Vins');

        $this->dataTransformerBuilder->setTransformerMethods(['getCollectionOption'])
                                     ->build('Criteria.BodyStyleId', 'Criteria.BodyStyleId');

        $this->dataTransformerBuilder->setTransformerMethods($this->flattenArrayAndValidateContentsAsNumbers)
                                     ->build('Criteria.ModelId', 'Criteria.ModelId');

        $this->dataTransformerBuilder->setTransformerMethods(['getCollectionOption'])
                                     ->build('Criteria.ColourId', 'Criteria.ColourId');

        $this->dataTransformerBuilder->setTransformerMethods(['getCollectionOption'])
                                     ->build('Criteria.FuelId', 'Criteria.FuelId');

        $this->dataTransformerBuilder->setTransformerMethods(['getCollectionOption'])
                                     ->build('Criteria.UpholsteryId', 'Criteria.UpholsteryId');

        $this->dataTransformerBuilder->setTransformerMethods(['getCollectionOption'])
                                     ->build('Criteria.TransmissionId', 'Criteria.TransmissionId');

        $this->dataTransformerBuilder->setTransformerMethods($this->flattenArrayAndValidateContentsAsNumbers)
                                     ->build('Criteria.RetailerId', 'Criteria.RetailerId');

        $this->dataTransformerBuilder->setTransformerMethods(['getMinMax'])
                                     ->build('Criteria.MonthlyPrice', 'Criteria.MonthlyPrice');

        $this->dataTransformerBuilder->setTransformerMethods(['getMinMax'])
                                     ->build('Criteria.OTR', 'Criteria.OTR');

        $this->dataTransformerBuilder->setTransformerMethods($this->flattenArrayAndValidateContentsAsNumbers)
                                     ->build('Criteria.RetailerGroupId', 'Criteria.RetailerGroupId');

        $this->dataTransformerBuilder->setTransformerMethods(['getRadius'])
                                     ->build('Criteria.Radius', 'Criteria.Radius');

        $this->dataTransformerBuilder->setTransformerMethods(['flattenArray'])
                                     ->build('Criteria.Keywords', 'Criteria.Keywords');

        $this->dataTransformerBuilder->setTransformerMethods($this->flattenArrayAndValidateContentsAsNumbers)
                                     ->build('Criteria.EngineId', 'Criteria.EngineId');

        $this->dataTransformerBuilder->setTransformerMethods(['getCollectionOption'])
                                     ->build('Criteria.EquipmentId', 'Criteria.EquipmentId');

        $this->dataTransformerBuilder->build('Criteria.VehicleType', 'Criteria.VehicleType');

        $this->dataTransformerBuilder->setTransformerMethods(['getMinMax'])
                                     ->build('Criteria.Mileage', 'Criteria.Mileage');

        $this->dataTransformerBuilder->setTransformerMethods(['getMinMax'])
                                     ->build('Criteria.Age', 'Criteria.Age');

        $this->dataTransformerBuilder->build('Criteria.Condition', 'Criteria.Condition');

        $this->dataTransformerBuilder->setTransformerMethods(['getMinMax'])
                                     ->build('Criteria.RetailPrice', 'Criteria.RetailPrice');

        $this->dataTransformerBuilder->setTransformerMethods($this->flattenArrayAndValidateContentsAsNumbers)
                                     ->build('Criteria.LineId', 'Criteria.LineId');
    }

    protected function buildFinanceCriteria(): void
    {
        $this->dataTransformerBuilder->build('Criteria.Finance.Key', 'FinanceCriteria.Key');
        $this->dataTransformerBuilder->build('Criteria.Finance.Name', 'FinanceCriteria.Name');
        $this->dataTransformerBuilder->build('Criteria.Finance.Type', 'FinanceCriteria.Type');

        $this->dataTransformerBuilder->setTransformerMethods(['convertToBool'])
                                     ->build('Criteria.Finance.IsDefault', 'FinanceCriteria.IsDefault');

        $this->dataTransformerBuilder->setTransformerMethods(['getOptions'])
                                     ->build('Criteria.Finance.Term', 'FinanceCriteria.Term.Options');

        $this->dataTransformerBuilder->build('Criteria.Finance.Deposit', 'FinanceCriteria.Deposit.Default');

        $this->dataTransformerBuilder->setTransformerMethods(['getOptions'])
                                     ->build('Criteria.Finance.Mileage', 'FinanceCriteria.Mileage.Options');

        $this->dataTransformerBuilder->setTransformerMethods(['getMinMax'])
                                     ->build('Criteria.Finance.MonthlyPrice', 'FinanceCriteria.MonthlyPrice');

        $this->dataTransformerBuilder->setTransformerMethods(['convertToBool'])
                                     ->build('Criteria.Finance.IsPersonalised', 'FinanceCriteria.IsPersonalised');

        $this->dataTransformerBuilder->build('Criteria.Finance.CustomerType', 'FinanceCriteria.CustomerType');
        $this->dataTransformerBuilder->build('Criteria.Finance.VehicleType', 'FinanceCriteria.VehicleType');

        $this->dataTransformerBuilder->setTransformerMethods(['convertToBool'])
                                     ->build('Criteria.Finance.agilitySelected', 'FinanceCriteria.agilitySelected');

        $this->dataTransformerBuilder->setTransformerMethods(['getMinMaxFinance'])
                                     ->build('Criteria.Finance.MonthlyPriceRange', 'FinanceCriteria.MonthlyPriceRange');
    }


    public function getArrayCleansedForNumbers(array $array):array
    {
        return ValidationHelper::filterArrayForNumbers($this->flattenArray($array));
    }

    public function getMinMax(array $array): array
    {
        $data['Min'] = ValidationHelper::filterForInteger($array['Min']);
        $data['Max'] = ValidationHelper::filterForInteger($array['Max']);
        return $data;
    }

    public function getMinMaxFinance(array $array): array
    {
        $data['financeMinRange'] = ValidationHelper::filterForInteger($array['Min']);
        $data['financeMaxRange'] = ValidationHelper::filterForInteger($array['Max']);
        return $data;
    }

    public function getPaging(array $array): array
    {
        $data = [
            'ResultsPerPage' => ValidationHelper::filterForInteger($array['ResultsPerPage']),
            'PageIndex' => ValidationHelper::filterForInteger($array['PageIndex']),
        ];
        return $data;
    }

    public function getRadius(array $array): array
    {
        $data = [
            'Postcode' => $array['Postcode'],
            'Distance' => ValidationHelper::filterForInteger($array['Distance']),
            'Lat' => ValidationHelper::filterForDecimal($array['Lat']),
            'Lon' => ValidationHelper::filterForDecimal($array['Lon']),
        ];
        return $data;
    }

    public function flattenArray(array $array): array
    {
        return ArrayHelper::flattenArray($array);
    }

    public function getOptions(array $array):array
    {
        $data = [];
        foreach($array as $option) {
            $data[] = [
                'IsDefault' => $this->convertToBool($option['IsDefault']),
                'Value' => ValidationHelper::filterForInteger($option['Value']),
            ];
        }
        return $data;
    }

    public function convertToBool($value): bool
    {
        return (bool) $value;
    }

    public function getCollectionOption(array $array, string $key): array
    {
        $data = [
            'id' => StringHelper::getLastPartOfString($key, '.'),
            'Values' => $this->getArrayCleansedForNumbers($array)
        ];
        return $data;
    }
}