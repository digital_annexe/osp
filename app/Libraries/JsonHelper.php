<?php
namespace App\Libraries;

class JsonHelper
{
    public static function convertJsonToArray(string $json): array
    {
        return json_decode($json, true);
    }

    public static function isJsonValid(string $string): bool
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}