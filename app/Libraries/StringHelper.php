<?php
namespace App\Libraries;

class StringHelper
{
    public static function getLastPartOfString(string $string, string $delimiter): string
    {
        return substr($string, strrpos($string, $delimiter) + 1);
    }

    public static function generateGuid(): string
    {
        return bin2hex(random_bytes(16));
    }
}