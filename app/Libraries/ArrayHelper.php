<?php
namespace App\Libraries;

class ArrayHelper
{
    public static function flattenArray(array $array): array
    {
        $return = array();
        foreach ($array as $key => $value) {
            if (is_array($value)){
                $return = array_merge($return, self::flattenArray($value));
            } else {
                $return[$key] = $value;
            }
        }
        return $return;
    }

    public static function doesKeyExist(string $key, array $array): bool
    {
        return strpos(json_encode($array), $key) > 0;
    }
}