<?php
namespace App\Libraries;

class ValidationHelper
{
    const NUMBERS_ONLY_PATTERN = "/[^0-9]/";

    public static function filterForInteger(string $value, int $defaultValue = 0): ?int
    {
        return self::pregMatchCleanse($value, $defaultValue, self::NUMBERS_ONLY_PATTERN);
    }

    /**
     * Cleanses the array, so that only values that are numbers are returned (not decimals)
     * @param array $array
     * @return array
     */
    public static function filterArrayForNumbers(array $array):array
    {
        $data =  array_filter($array, function($value){
            return !preg_match (self::NUMBERS_ONLY_PATTERN, $value);
        });
        return $data;
    }

    public static function filterForNumbers(string $value): int
    {
        return preg_replace(self::NUMBERS_ONLY_PATTERN, '', $value);
    }

    public static function filterForDecimal(string $value, $defaultValue = 0)
    {
        return (is_numeric($value)) ? $value : $defaultValue;
    }

    protected static function pregMatchCleanse(string $value, $defaultValue, string $pattern): string
    {
        !preg_match ($pattern, $value) ? $finalValue = $value : $finalValue = $defaultValue;
        return $finalValue;
    }
}