<?php
namespace App;

interface CacheHandlerInterface
{
    public function set(array $data): void;

    public function get(): array;

    public function exists(): bool;

    public function delete(): void;
}