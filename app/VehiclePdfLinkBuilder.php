<?php
namespace App;

use App\Services\DataTransformerBuilder;
use App\UrlTransformerInterface;

class VehiclePdfLinkBuilder implements UrlTransformerInterface
{
    protected $data = [];

    protected $baseUri;

    public function __construct(array $data, string $baseUri)
    {
        $this->data = [
            'Vehicle' => $data['General'],
            'Finance' => $data['Finance']['FinanceCalculate']['VehicleResults'][0]['FinanceProductResults'][0]
        ];
        $this->baseUri = $baseUri;
    }

    public function transform(): string
    {
        $dataTransformerBuilder = new DataTransformerBuilder($this->data);
        $transformer = new VehicleDetailPdfDataTransformer($dataTransformerBuilder);
        $options = $transformer->transform();
        $url = $this->baseUri . '/' . implode('/', array_values($options));
        return $url;
    }
}