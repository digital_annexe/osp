<?php
namespace App\Services;

use App\DataTransformerInterface;

interface DataTransformerBuilderInterface
{
    public function setDataTransformer(DataTransformerInterface $dataTransformer): DataTransformerBuilderInterface;

    public function setBase(array $transformedBase): DataTransformerBuilderInterface;

    public function render(): array;

    public function setTransformerMethods(array $dataTransformerMethods): DataTransformerBuilderInterface;

    /**
     * @param string $sourceKey : array path (key) from source, that is being copied
     * @param string $destinationKey : array path (key) in destination, that is being assigned
     *
     * The key for both $sourceKey and $destinationKey is a dot separated path (for multidimensional arrays), e.g.
     * $data['primary'] is represented as 'primary'
     * $data['primary']['secondary'] is represented as 'primary.secondary'
     */
    public function build(string $sourceKey, string $destinationKey): void;
}