<?php
namespace App\Services;

use GuzzleHttp\Psr7\Response;
use Log;
use GuzzleHttp\Psr7\Uri;
use App\Libraries\JsonHelper;
use App\Http\Controllers\LoggerGuidTrait;

class ApiLogger implements ApiLoggerInterface
{
    use LoggerGuidTrait;

    protected $baseUri;
    protected $url;

    /**
     * ApiLogger constructor.
     * @param Uri $baseUri
     * @param string $uri
     */
    public function __construct(Uri $baseUri, string $uri)
    {
        $this->baseUri = $baseUri;
        $this->url = $this->getUrl($uri);
    }

    public function logRequest(): void
    {
        $info = [
            'type' => 'request',
            'guid' => $this->getLoggerGuid()
        ];
        Log::info($this->url, $info);
    }

    public function logResponse(Response $response): void
    {
        $info = [
            'type' => 'response',
            'body' => JsonHelper::convertJsonToArray($response->getBody()->getContents()),
            'guid' => $this->getLoggerGuid()
        ];
        Log::info($this->url, $info);
    }

    public function logMessage(string $message): void
    {
        $info = [
            'message' => $message,
            'guid' => $this->getLoggerGuid()
        ];
        Log::info($this->url, $info);
    }

    protected function getUrl(string $uri): string
    {
        $url = "{$this->baseUri->getScheme()}//{$this->baseUri->getHost()}/{$uri}" .
               (($this->baseUri->getQuery() !== null) ? "/{$this->baseUri->getQuery()}" : null);
        return $url;
    }
}