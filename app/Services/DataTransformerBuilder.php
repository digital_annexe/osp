<?php
namespace App\Services;

use App\DataTransformerInterface;

class DataTransformerBuilder implements DataTransformerBuilderInterface
{
    protected $data = [];

    protected $transformedData = [];

    protected $dataTransformerMethods = [];

    protected $dataTransformer;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function setDataTransformer(DataTransformerInterface $dataTransformer): DataTransformerBuilderInterface
    {
        $this->dataTransformer = $dataTransformer;
        return $this;
    }

    public function setBase(array $transformedBase): DataTransformerBuilderInterface
    {
        $this->transformedData = $transformedBase;
        return $this;

    }

    public function render(): array
    {
        return $this->transformedData;
    }

    public function setTransformerMethods(array $dataTransformerMethods): DataTransformerBuilderInterface
    {
        $this->dataTransformerMethods = $dataTransformerMethods;
        return $this;
    }

    /**
     * @param string $sourceKey : array path (key) from source, that is being copied
     * @param string $destinationKey : array path (key) in destination, that is being assigned
     *
     * The key for both $sourceKey and $destinationKey is a dot separated path (for multidimensional arrays), e.g.
     * $data['primary'] is represented as 'primary'
     * $data['primary']['secondary'] is represented as 'primary.secondary'
     */
    public function build(string $sourceKey, string $destinationKey): void
    {
        $value = $this->get($sourceKey, $this->data);

        if (!empty($value)) {
            $this->transform($destinationKey, $value);
        } elseif ($value === false) {
            $this->set($destinationKey, $this->transformedData, false);
        }
        $this->dataTransformerMethods = [];
    }

    protected function get($path, array $array) {
        $path = explode('.', $path);
        $temp =& $array;

        foreach($path as $key) {
            $temp =& $temp[$key];
        }
        return $temp;
    }

    protected function set($path, array &$array = [], $value = null): void {
        $path = explode('.', $path);
        $temp =& $array;

        foreach($path as $key) {
            $temp =& $temp[$key];
        }
        $temp = $value;
    }

    protected function transform(string $destinationKey, $value): void
    {
        if (is_null($this->dataTransformerMethods)) {
            $this->set($destinationKey, $this->transformedData, $value);
        } else {
            $transformedValues = $this->applyTransformerMethods($value, $destinationKey);
            $this->set($destinationKey, $this->transformedData, $transformedValues);
        }
    }

    protected function applyTransformerMethods($data, string $key = null)
    {
        $transformedData = $data;
        foreach($this->dataTransformerMethods as $dataTransformerMethod) {
            $transformedData = $this->dataTransformer->$dataTransformerMethod($transformedData, $key);
        }
        return $transformedData;
    }
}