<?php
namespace App\Services;

use App\Services\APIServiceInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Client;
use App\Libraries\JsonHelper;
use Log;
use App\Services\ApiLogger;

class ApiProxyService implements APIServiceInterface
{
    const BASIC_AUTH = 'basic_auth';
    const BEARER_TOKEN_AUTH = 'bearer_token';
    const CONTENT_TYPE_JSON = 'application/json';

    protected $config = [];

    /**
     * RrapApiService constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $uri
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function get(string $uri = null, array $data = null): array
    {
        try {
            $logger = $this->getLogger($uri);
            $logger->logRequest();
            $response = $this->getClient()->request('get', $uri);
            $logger->logResponse($response);

        } catch (GuzzleException $exception) {
            $message = "There was a problem accessing GET {$this->config['base_uri']}/{$uri} :" .
                        $exception->getMessage();
            throw new \Exception($message);
        }
        return JsonHelper::convertJsonToArray($response->getBody());
    }

    /**
     * @param string $uri
     * @param array $data
     * @return array|null
     * @throws \Exception
     */
    public function post(string $uri, array $data): ?array
    {
        switch ($this->config['content_type']) {
            case self::CONTENT_TYPE_JSON;
            default;
                return $this->postJson($uri, $data);
                break;
        }
    }

    protected function postJson(string $uri, array $data): ?array
    {
        try {
            $logger = $this->getLogger($uri);
            $logger->logRequest();
            $response = $this->getClient()->request('post', $uri, ['json' => $data]);
            $logger->logResponse($response);

        } catch (GuzzleException $exception) {
            $message = "There was a problem accessing POST {$this->config['base_uri']}/{$uri} :" .
                $exception->getMessage();
            throw new \Exception($message);
        }
        return JsonHelper::convertJsonToArray($response->getBody());
    }

    protected function getClient(): Client
    {
        $baseConfig = [
            'base_uri' => $this->config['base_uri'],
            'headers' => [
                'Accept' => $this->config['content_type'],
                'Content-Type' => $this->config['content_type']
            ]
        ];
        $finalConfig = $this->prepareConfig($baseConfig);
        return new Client($finalConfig);
    }

    protected function prepareConfig(array $baseConfig): array
    {
        $finalConfig = $baseConfig;
        switch ($this->config['auth_type']) {
            case self::BASIC_AUTH;
                $finalConfig['auth'] =                 [
                    $this->config['auth_username'],
                    $this->config['auth_password']
                ];
                break;

            case self::BEARER_TOKEN_AUTH;
                $finalConfig['headers']['Authorization'] = 'Bearer ' . $this->config['auth_token'];
                break;

            default;
                break;
        }
        return $finalConfig;
    }

    protected function getLogger(string $uri): ApiLogger
    {
        $logger = new ApiLogger($this->getClient()->getConfig('base_uri'), $uri);
        return $logger;
    }
}