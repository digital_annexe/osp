<?php
namespace App\Services;

interface APIServiceInterface
{
    public function get(string $uri = null, array $data = null): array;

    public function post(string $uri, array $data): ?array;

}