<?php
namespace App\Services;

use Contentful\Delivery\Client;

interface CmsApiServiceInterface
{
    public function getPageByName(string $name) : array;

    public function getClient() : Client;
}