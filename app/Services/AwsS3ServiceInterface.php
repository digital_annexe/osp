<?php
namespace App\Services;

use Aws\S3\S3Client;

interface AwsS3ServiceInterface
{
    public function getFile(string $filepath): array;

    public function getClient(): S3Client;
}