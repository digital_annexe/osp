<?php
namespace App\Services;

use GuzzleHttp\Psr7\Response;

interface ApiLoggerInterface
{
    public function logRequest(): void;

    public function logMessage(string $message): void;
}