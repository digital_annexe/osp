<?php
namespace App\Services;

use Contentful\Delivery\Client;
use Contentful\Delivery\Query;
use Contentful\Core\Exception\NotFoundException;
use App\Services\CmsLogger;
use Contentful\Delivery\Resource\Entry;
use Contentful\Core\File\FileInterface;

class CmsApiService implements CmsApiServiceInterface
{
    /**
     * @var Client
     */
    protected $client = null;

    /**
     * CmsApiService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getPageByName(string $name): array
    {
        return $this->getSearchItemByName($name, 'pages', 'name');
    }

    public function getCuratedSearchPageByName(string $name): array
    {
        return $this->getSearchItemByName($name, 'curatedSearch', 'title');
    }

    public function showSuperCarsPage(string $name): array
    {
        return $this->getSearchItemByName($name, 'superCars', 'preFiltered');
    }

    public function getByQuery(Query $query):array
    {
        return $this->get($query);
    }

    public function getClient() : Client
    {
        return $this->client;
    }

    protected function getSearchItemByName(string $name, string $type, string $field): array
    {
        $query = (new Query())
            ->setContentType($type)
            ->where("fields.{$field}[match]",  $name)
            ->setInclude(4);
        $main = $this->get($query);

        $image = $this->getImage($main);
        if (!empty($image)) {
            $main['resolved']['image'] = $image;
        }

        $resolvedFeatures = $this->getPageFeatures($main);
        if (!empty($resolvedFeatures)) {
            $main['resolved']['features'] = $resolvedFeatures;
        }
        return $main;
    }

    protected function get(Query $query):array
    {
        try {
            $logger = $this->getLogger($query->getQueryString());
            $logger->logRequest();
            $entries = $this->client->getEntries($query)->jsonSerialize();
            $logger->logResponse($entries);

            return $entries;
        } catch (NotFoundException $exception) {
            throw $exception;
        }
    }

    protected function getLogger(string $query): CmsLogger
    {
        $logger = new CmsLogger($query);
        return $logger;
    }

    protected function getPageFeatures(array $main): array
    {
        $features = [];
        if (!empty($main) && isset($main['items'][0]['features'])) {
            foreach ($main['items'][0]['features'] as $feature) {
                $features[] = $feature->all();
            }
        }
        return $features;
    }

    protected function getImage(array $main): ?FileInterface
    {
        $image = null;
        if (isset($main['items'][0]['image'])) {
            $image = $main['items'][0]['image']->getFile();
        }
        return $image;
    }
}