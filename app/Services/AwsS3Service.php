<?php
namespace App\Services;

use Aws\S3\S3Client;

class AwsS3Service implements AwsS3ServiceInterface
{
    /**
     * @var S3Client
     */
    protected $client;

    /**
     * AwsS3Service constructor.
     * @param $client
     */
    public function __construct(S3Client $client)
    {
        $this->client = $client;
    }


    public function getFile(string $filepath): array
    {
        $this->getClient()->registerStreamWrapper();
        $finalPath = 's3://' . env('AWS_BUCKET') . "/{$filepath}";
        $data = explode(PHP_EOL, file_get_contents($finalPath));
        return $data;
    }

    public function getClient(): S3Client
    {
        return $this->client;
    }
}