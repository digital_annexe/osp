<?php

namespace App\Services;

use App\Libraries\JsonHelper;
use Log;
use Contentful\Core\Resource\ResourceArray;
use App\Http\Controllers\LoggerGuidTrait;

class CmsLogger implements ApiLoggerInterface
{
    use LoggerGuidTrait;

    const SERVICE = 'Contentful';

    /** @var string  */
    protected $queryString;

    /**
     * CmsLogger constructor.
     * @param string $queryString
     */
    public function __construct(string $queryString)
    {
        $this->queryString = $queryString;
    }

    public function logRequest(): void
    {
        $info = [
            'type' => 'request',
            'guid' => $this->getLoggerGuid()
        ];
        Log::info($this->getQueryIdentifier(), $info);
    }

    public function logResponse(array $response): void
    {
        $info = [
            'type' => 'response',
            'body' => JsonHelper::convertJsonToArray(json_encode($response, true)),
            'guid' => $this->getLoggerGuid()
        ];
        Log::info($this->getQueryIdentifier(), $info);
    }

    public function logMessage(string $message): void
    {
        $info = [
            'message' => $message,
            'guid' => $this->getLoggerGuid()
        ];
        Log::info($this->getQueryIdentifier(), $info);
    }

    protected function getQueryIdentifier()
    {
        return self::SERVICE . " : {$this->queryString}";
    }
}