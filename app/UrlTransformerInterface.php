<?php

namespace App;

interface UrlTransformerInterface
{
    public function transform(): string;
}