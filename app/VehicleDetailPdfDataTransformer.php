<?php
namespace App;

use App\Services\DataTransformerBuilderInterface;
use App\Libraries\ValidationHelper;

class VehicleDetailPdfDataTransformer implements DataTransformerInterface
{
    protected $data = [];

    protected $dataTransformerBuilder;

    public function __construct(DataTransformerBuilderInterface $dataTransformerBuilder)
    {
        $this->dataTransformerBuilder = $dataTransformerBuilder;
        $this->dataTransformerBuilder->setDataTransformer($this)
                                     ->setBase($this->getDefaults());
    }

    protected function getDefaults(): array
    {
        $data = [
            'VehicleId' => null,
            'RetailerId' => null,
            'FinanceProduct' => null,
            'Term' => null,
            'Deposit' => 0,
            'RegularPayment' => 0,
            'AdvanceRentals' => 0,
            'AnnualMileage' => 0,
            'IsPersonalised' => 0,
            'PartExchange' => 0,
            'Settlement' => 0,
            'CustomerType' => null,
        ];
        return $data;
    }

    public function transform(): array
    {
        $this->dataTransformerBuilder->setTransformerMethods(['cleanseForNumbers'])
                                     ->build('Vehicle.Id', 'VehicleId');

        $this->dataTransformerBuilder->setTransformerMethods(['cleanseForNumbers'])
                                     ->build('Vehicle.Retailer.Id', 'RetailerId');

        $this->dataTransformerBuilder->build('Finance.Key', 'FinanceProduct');

        $this->dataTransformerBuilder->setTransformerMethods(['cleanseForNumbers'])
                                     ->build('Finance.QuoteDto.DurationOfAgreement', 'Term');

        $this->dataTransformerBuilder->setTransformerMethods(['filterForDecimal'])
                                     ->build('Finance.QuoteDto.TotalDepositContribution', 'Deposit');

        $this->dataTransformerBuilder->setTransformerMethods(['cleanseForNumbers'])
                                     ->build('Finance.QuoteDto.NumberOfPayments', 'RegularPayment');

        $this->dataTransformerBuilder->setTransformerMethods(['cleanseForNumbers'])
                                     ->build('Finance.QuoteDto.AdvanceRentals', 'AdvanceRentals');

        $this->dataTransformerBuilder->setTransformerMethods(['cleanseForNumbers'])
                                     ->build('Finance.QuoteDto.AnnualMileage', 'AnnualMileage');

        $this->dataTransformerBuilder->setTransformerMethods(['cleanseForNumbers'])
                                     ->build('Finance.QuoteDto.PartExchange', 'PartExchange');

        $this->dataTransformerBuilder->setTransformerMethods(['cleanseForNumbers'])
                                     ->build('Finance.QuoteDto.Settlement', 'Settlement');

        $this->dataTransformerBuilder->build('Finance.QuoteDto.CustomerType', 'CustomerType');
        return $this->dataTransformerBuilder->render();
    }

    public function cleanseForNumbers(string $value):int
    {
        return ValidationHelper::filterForNumbers($value);
    }

    public function filterForDecimal(string $value)
    {
        return ValidationHelper::filterForDecimal($value);
    }
}