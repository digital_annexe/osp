<?php
namespace App;

use Illuminate\Support\Facades\Cache;

class CacheHandler implements CacheHandlerInterface
{
    protected $key;

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    public function set(array $data): void
    {
        $this->delete();
        Cache::forever($this->key, $data);
    }

    public function exists(): bool
    {
        return Cache::has($this->key);
    }

    public function get(): array
    {
        return Cache::get($this->key);
    }

    public function delete(): void
    {
        Cache::forget($this->key);
    }
}