#OSP Proxy API Service
A REST API service which acts as a proxy between the [RRAP](https://uat-api-vehiclesearch-mb.rapp-customers.co.uk/metadata) and [Contentful](https://www.contentful.com/developers/docs/references/content-delivery-api/) API services.
Based on Lumen PHP Framework.

##Installation
1. Download [repo](https://bitbucket.org/digital_annexe/osp/src/master/) into web root.

2. Copy appropriate (dev or prod) ENV file into project root. This file is not in the repo and has to be obtained through management.

3. Run __composer install__

##Configuration
###Main configuration 
This is held in the ENV file and the main configuration file (config/options.php).

###Routing
routes/web.php

###Controllers
app/Http/Controllers

###CORS
app/Http/Middleware/CorsMiddleware.php

###Unit tests
No unit tests have been written


 